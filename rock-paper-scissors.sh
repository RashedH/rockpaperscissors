#!/bin/bash
ROCK="rock"
PAPER="paper"
SCISSORS="scissors"

function main {
  # read player 1's guess
  echo -p "Player 1 guess: "
  read -s player1
  # read player 2's guess
  echo -p "Player 2 guess: "
  read -s player2
  
  # compare guesses and announce winner
  if [[ $player1 == $player2 ]]; then
    echo "Its a Tie"
  elif [[ $player1 == $ROCK && $player2 == $SCISSORS ]]; then
    echo "Player 1 Wins"
  elif [[ $player1 == $SCISSORS && $player2 == $ROCK ]]; then
    echo "Player 2 Wins"
  elif [[ $player1 == $SCISSORS && $player2 == $PAPER ]]; then
    echo "Player 1 Wins"
  elif [[ $player1 == $PAPER && $player2 == $SCISSORS ]]; then
    echo "Player 2 Wins"
  elif [[ $player1 == $PAPER && $player2 == $ROCK ]]; then
    echo "Player 1 Wins"
  elif [[ $player1 = $ROCK && $player2 == $PAPER ]]; then
    echo "Player 2 Wins"
  fi
    
  echo "Play again? (y/n): "
  read choice
  if [ $choice == "y" ] || [ $choice == "Y" ]; then
    main
  fi
}

main